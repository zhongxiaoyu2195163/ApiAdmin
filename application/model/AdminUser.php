<?php
/**
 * @since   2017-11-02
 * @author  zhaoxiang <zhaoxiang051405@gmail.com>
 */

namespace app\model;


class AdminUser extends Base {
    public function getCreateTimeAttr($value) {
        return strtotime($value);
    }

    public function getLastLoginTime($value) {
        return strtotime($value);
    }
}
