<?php
/**
 * Api路由
 */

use think\Route;

Route::group('api', function () {
    Route::miss('api/Miss/index');
});
$afterBehavior = [
    '\app\api\behavior\ApiAuth',
    '\app\api\behavior\ApiPermission',
    '\app\api\behavior\RequestFilter'
];
Route::rule('api/5ba494f37424c','api/BuildToken/getAccessToken', 'POST', ['after_behavior' => $afterBehavior]);Route::rule('api/5ba4a6bb3724f','api/User/getUserInfo', 'POST', ['after_behavior' => $afterBehavior]);Route::rule('api/5ba5aa45555b7','api/User/getChatLogAll', 'POST', ['after_behavior' => $afterBehavior]);Route::rule('api/5ba5de587669f','api/User/sendMsgForUser', 'POST', ['after_behavior' => $afterBehavior]);Route::rule('api/5ba5f107a0cd5','api/User/sendMediaForUser', 'POST', ['after_behavior' => $afterBehavior]);