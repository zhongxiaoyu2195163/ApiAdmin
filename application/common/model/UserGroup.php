<?php
namespace app\common\model;

use think\Model;
use think\Db;

class UserGroup extends Model
{
    protected $insert = ['create_time'];

    protected function setCreateTimeAttr(){
        return date('Y:m:d H:i:s');
    }

    public function getLabelTopClass($auid){
        $result = [];
        $label_result_arr = Db::name('user_group')->where('auid',$auid)->select();
        foreach($label_result_arr as $v){
            if($v['pid'] == 0){
                $v['s_label'] = [];
                foreach($label_result_arr as $vo){
                    if($v['id'] == $vo['pid']){
                        $v['s_label'][] = $vo;
                    }
                }
                $result[] = $v;
            }
        }
        return $result;
    }
}