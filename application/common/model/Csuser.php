<?php
namespace app\common\model;

use think\Model;

class Csuser extends Model
{
    protected $insert = ['create_time'];

    protected function setCreateTimeAttr(){
        return time();
    }

    public function get_valid_count_and_online_count($wechat){
        $return = $this->field("count(*) as valid_count,(SELECT count(*) FROM think_csuser where auid = {$wechat['id']} AND status = 1 AND type = 1) as online_count")
                    ->where('auid',$wechat['id'])
                    ->where('status',1)
                    ->find()->toArray();
        return $return;
    }
}