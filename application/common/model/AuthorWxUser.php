<?php
namespace app\common\model;

use think\Model;

class AuthorWxUser extends Model
{
    // protected $insert = ['create_time'];

    // protected function setAttrCreateTime(){
    //     return time();
    // }

    public function get_wx_list($where){
        $return = $this->alias('w')
                        ->field('w.id,w.wxname,w.wxtype,w.principal_name,think_admin_user.username as admin_name,w.headerpic')
                        ->where($where)
                        ->join('think_admin_user','w.uid = think_admin_user.id')
                        ->select();
        return $return;
    }
}