<?php
namespace app\common\model;

use think\Model;

class Bespeak extends Model
{
    protected $insert = ['create_time'];

    protected function setCreateTimeAttr(){
        return time();
    }

    protected function getCreateTimeAttr($value){
        return date('Y-m-d H:i:s',$value);
    }

    protected function getSendTimeAttr($value){
        return date('Y-m-d H:i:s',$value);
    }
}