<?php
namespace app\common\model;

use think\Model;
use think\Session;

class Users extends Model
{
    protected $insert = ['auid'];
    protected $wx_user;

    protected function initialize(){
        parent::initialize();
        $this->wx_user = Session::get('wechat_info');
    }

    /**
     * 创建时间
     * @return bool|string
     */
    // protected function setCreateTimeAttr()
    // {
    //     return date('Y-m-d H:i:s');
    // }

    protected function setAuidAttr($value = ''){
        if(!empty($value)) return $value;
        return $this->wx_user['id'];
    }

    protected function setTagidListAttr($value){
        return serialize($value);
    }

    protected function getTagidListAttr($value){
        return unserialize($value);
    }
}