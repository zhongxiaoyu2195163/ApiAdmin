<?php

/**
 * tpshop
 * ============================================================================
 * 版权所有 2017-2027 深圳搜豹网络科技有限公司，并保留所有权利。
 * 网站地址: http://www.tp-shop.cn
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用 .
 * 不允许对程序代码以任何形式任何目的的再发布。
 * 如果商业用途务必到官方购买正版授权, 以免引起不必要的法律纠纷.
 * ============================================================================
 * author: lhb
 * Date: 2017-5-8
 */
namespace app\common\util;

use app\common\model\WxUser;
use app\common\model\AuthorWxUser;
use think\Config;
use think\Db;

class WechatThirdUtil
{
    private $config = [];    //第三方开发平台配置
    private $author_config = []; //授权公众号配置
    private $errorMsg = '';  //错误字符串信息
    private $debug = false;   //是否开启调试
    private $tagsMap = null; //粉丝标签映射
    private $wx_user;
    private $author_wx_user;
    
    /**
     * @param config 第三方平台信息
     * @param author_config 授权公众号信息
     */
    public function __construct($config,$author_config)
    {
        $this->wx_user = new WxUser;
        $this->author_wx_user = new AuthorWxUser;
        $this->config = $config;
        $this->author_config = $author_config;
    }
    
    public function getError() 
    {
        return $this->errorMsg;
    }
    
    private function setError($error)
    {
        if (!is_string($error)) {
            $error = json_encode($error, JSON_UNESCAPED_UNICODE);
        }
        $this->errorMsg = $error;
    }
    
    public function isDedug()
    {
        return $this->debug;
    }
    
    public function logDebugFile($content)
    {
        if (!$this->debug) {
            return;
        }
        if (!is_string($content)) {
            $content = json_encode($content, JSON_UNESCAPED_UNICODE);
        }
        file_put_contents("./wechat.log", date('Y-m-d H:i:s').' -- '.$content."\n", FILE_APPEND);
    }

    public function cryptMsg($temp){
        $wechat = $this->config;
        include_once(EXTEND_PATH.'wxcrypt/wxBizMsgCrypt.php');
        //encodingAesKey和token均为申请三方平台是所填写的内容 
        $encodingAesKey = $wechat['encoding_aes_key'];
        $token = $wechat['w_token'];
        $appId = $wechat['appid'];
        $pc = new \WXBizMsgCrypt($token, $encodingAesKey, $appId);
        $encryptMsg = ''; 
        $errCode =$pc->encryptMsg($temp, $_GET['timestamp'], $_GET['nonce'], $encryptMsg); 

        if($errCode == 0){ 
            return $encryptMsg; 
        }else{
            return false;
        }
    }
    
    /**
     * http请求
     * @param type $url
     * @param type $method
     * @param type $fields
     * @return 
     */
    private function httpRequest($url, $method = 'GET', $fields = [])
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);

        $method = strtoupper($method);
        if ($method == 'GET' && !empty($fields)) {
            is_array($fields) && $fields = http_build_query($fields);
            $url = $url . (strpos($url,"?")===false ? "?" : "&") . $fields;
        }
        curl_setopt($ch, CURLOPT_URL, $url);

        if ($method != 'GET') {
            curl_setopt($ch, CURLOPT_POST, true);
            if (!empty($fields)) {
                $hadFile = false;
                if (is_array($fields)) {
                    /* 支持文件上传 */
                    if (class_exists('\CURLFile')) {
                        curl_setopt($ch, CURLOPT_SAFE_UPLOAD, true);
                        foreach ($fields as $key => $value) {
                            if ($this->isPostHasFile($value)) {
                                $fields[$key] = new \CURLFile(realpath(ltrim($value, '@')));
                                $hadFile = true;
                            }
                        }
                    } elseif (defined('CURLOPT_SAFE_UPLOAD')) {
                        if ($this->isPostHasFile($value)) {
                            curl_setopt($ch, CURLOPT_SAFE_UPLOAD, false);
                            $hadFile = true;
                        }
                    }
                }
                $fields = (!$hadFile && is_array($fields)) ? http_build_query($fields) : $fields;
                curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
            }
        }

        /* 关闭https验证 */
        if ("https" == substr($url, 0, 5)) {
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        }

        $content = curl_exec($ch);
        curl_close($ch);
        return $content;
    }

    private function isPostHasFile($value)
    {
        if (is_string($value) && strpos($value, '@') === 0 && is_file(realpath(ltrim($value, '@')))) {
            return true;
        }
        return false;
    }

    /**
     * 下载媒体文件
     * @param string url
     */
    private function downloadMediaSource($url){
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_NOBODY, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        /* 关闭https验证 */
        if ("https" == substr($url, 0, 5)) {
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        }
        $content = curl_exec($ch);
        $http_info = curl_getinfo($ch);
        curl_close($ch);
        $result = array_merge(array('header' => $http_info), array('body' => $content));
        return $result;
    }
    
    /**
     * 专门用来检查微信接口返回值的。
     */
    private function requestAndCheck($url, $method = 'GET', $fields = [])
    {
        $return = $this->httpRequest($url, $method, $fields);
        if ($return === false) {
            if ($this->debug) {
                $this->setError("http请求出错！ " . Requests::$error);
            } else {
                $this->setError("http请求出错！");
            }
            return false;
        }

        $wxdata = json_decode($return, true);
        $this->debug && $this->logDebugFile(['url' => $url,'fields' => $fields,'wxdata' => $wxdata]);
        if (isset($wxdata['errcode']) && $wxdata['errcode'] != 0) {
            file_put_contents('./requestMsgError.log','['.date("Y-m-d H:i:s").'] ReturnMsg:'.json_encode(['url' => $url,'fields' => $fields,'wxdata' => $wxdata, 'return' => $return],JSON_UNESCAPED_UNICODE)."\n",FILE_APPEND);
            if ($wxdata['errcode'] == 40001) {
                $this->wx_user->save(['web_expires' => 0],['id'=>$this->config['id']]);//token错误
            }
            if ($this->debug) {
                $this->setError("微信错误代码：{$wxdata['errcode']};<br>错误信息：{$wxdata['errmsg']}<br>请求链接：$url");
            } else {
                $this->setError("操作失败，微信错误码：{$wxdata['errcode']};");
            }
            return false;
            // return $return;
        }

        if (strtoupper($method) === 'GET' && empty($wxdata)) {
            if ($this->debug) {
                $this->setError("微信http请求返回为空！请求链接：$url");
            } else {
                $this->setError("微信http请求返回为空！操作失败");
            }
            return false;
        }

        return $wxdata;
    }

    /**
     * 获取access_token
     * @param array $wechat wechar 公众号信息，查表可得
     * @return string
     */
    public function getAccessToken()
    {
        $wechat = $this->config;
        if (empty($wechat)) {
            $this->setError("公众号不存在！");
            return false;
        } 
            
        //判断是否过了缓存期
        $expire_time = $wechat['web_expires'];
        if($expire_time > time()){
           return $wechat['web_access_token'];
        }
        
        $appid = $wechat['appid'];
        $appsecret = $wechat['appsecret'];
        $url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid={$appid}&secret={$appsecret}";
        $return = $this->requestAndCheck($url, 'GET');
        if ($return === false) {
            return false;
        }
        
        $web_expires = time() + 7000; // 提前200秒过期
        $this->wx_user->save(['web_access_token'=>$return['access_token'], 'web_expires'=>$web_expires],['id'=>$wechat['id']]);
        $this->config['web_access_token'] = $return['access_token'];
        $this->config['web_expires'] = $web_expires;
        
        return $return['access_token'];
    }
    
    /**
     * 获取粉丝详细信息
     * @param type $openid
     * @param $access_token 如果为null，自动获取
     * @return array
     */
    public function getFanInfo($openid)
    {
        $access_token = $this->flushAuthorCode();
        if (!$access_token) {
            return false;
        }
        
        $url ="https://api.weixin.qq.com/cgi-bin/user/info?access_token={$access_token}&openid={$openid}&lang=zh_CN";
        $return = $this->requestAndCheck($url, 'GET');
        if ($return === false) {
            return false;
        }


        
        /* $wxdata[]元素：
         * subscribe	用户是否订阅该公众号标识，值为0时，代表此用户没有关注该公众号，拉取不到其余信息。
         * openid	用户的标识，对当前公众号唯一
         * nickname	用户的昵称
         * sex	用户的性别，值为1时是男性，值为2时是女性，值为0时是未知
         * city	用户所在城市
         * country	用户所在国家
         * province	用户所在省份
         * language	用户的语言，简体中文为zh_CN
         * headimgurl	用户头像，最后一个数值代表正方形头像大小（有0、46、64、96、132数值可选，0代表640*640正方形头像），用户没有头像时该项为空。若用户更换头像，原有头像URL将失效。
         * subscribe_time	用户关注时间，为时间戳。如果用户曾多次关注，则取最后关注时间
         * unionid	只有在用户将公众号绑定到微信开放平台帐号后，才会出现该字段。
         * remark	公众号运营者对粉丝的备注，公众号运营者可在微信公众平台用户管理界面对粉丝添加备注
         * groupid	用户所在的分组ID（兼容旧的用户分组接口）
         * tagid_list	用户被打上的标签ID列表
         */
        $return['sex_name'] = isset($return['sex']) ? $this->sexName($return['sex']) : '';
        return $return;
    }

    /**
     * sex_id 用户的性别，值为1时是男性，值为2时是女性，值为0时是未知
     */
    public function sexName($sex_id)
    {
        if ($sex_id == 1) {
            return '男';
        } else if ($sex_id == 2) {
            return '女';   
        }
        return '未知';
    }

    /**
     * 创建标签
     * @param type tag
     */
    public function createTag($name){
        $access_token = $this->flushAuthorCode();
        if (!$access_token) {
            return false;
        }

        $url = "https://api.weixin.qq.com/cgi-bin/tags/create?access_token={$access_token}";

        $post_arr = [
            'tag'=>$name
        ];

        $post = json_encode($post_arr,JSON_UNESCAPED_UNICODE);

        $return = $this->requestAndCheck($url,'POST',$post);

        if($return === false){
            return false;
        }

        return  $return;
    }

    /**
     * 创建分组
     * @param type tag
     */
    public function createGroups($name){
        $access_token = $this->flushAuthorCode();
        if (!$access_token) {
            return false;
        }

        $url = "https://api.weixin.qq.com/cgi-bin/groups/create?access_token={$access_token}";

        $post_arr = [
            'group'=>[
                'name' => $name
            ]
        ];

        $post = json_encode($post_arr,JSON_UNESCAPED_UNICODE);

        $return = $this->requestAndCheck($url,'POST',$post);

        if($return === false){
            return false;
        }

        return  $return;
    }

    /**
     * 编辑标签
     * @param type id
     * @param type tag
     * @return boolean
     */
    public function updateTag($id,$tag){
        $access_token = $this->flushAuthorCode();
        if (!$access_token) {
            return false;
        }

        $url = "https://api.weixin.qq.com/cgi-bin/tags/update?access_token={$access_token}";

        $post_arr = [
            'tag'=>[
                'id'=>$id,
                'name'=>$tag,
            ],
        ];

        $post = json_encode($post_arr,JSON_UNESCAPED_UNICODE);

        $return = $this->requestAndCheck($url,'POST',$post);
        if($return === false){
            return false;
        }

        return $return;
    }

    /**
     * 删除标签
     */
    public function deleteTag($id){
        $access_token = $this->flushAuthorCode();
        if (!$access_token) {
            return false;
        }

        $url = "https://api.weixin.qq.com/cgi-bin/tags/delete?access_token={$access_token}";

        $post_arr = [
            'tag'=>[
                'id'=>$id,
            ],
        ];

        $post = json_encode($post_arr,JSON_UNESCAPED_UNICODE);

        $return = $this->requestAndCheck($url ,'POST' ,$post);
        if($return === false){
            return false;
        }

        return $return;
    }

    /**
     * 获取标签下粉丝列表
     */
    public function getTagUsersList($tagid,$next_openid = ''){
        $access_token = $this->flushAuthorCode();
        if (!$access_token) {
            return false;
        }

        $url = "https://api.weixin.qq.com/cgi-bin/user/tag/get?access_token={$access_token}";

        $post_arr = [
            "tagid"=>$tagid,
            "next_openid"=>$next_openid,
        ];

        $post = json_encode($post_arr,JSON_UNESCAPED_UNICODE);

        $return = $this->requestAndCheck($url,'GET',$post);
        if($return === false){
            return false;
        }

        return $return;
    }

    /**
     * 移动用户分组
     */
    public function moveUsersTag($openid,$tagid){
        $access_token = $this->flushAuthorCode();
        if (!$access_token) {
            return false;
        }

        $url = "https://api.weixin.qq.com/cgi-bin/groups/members/update?access_token={$access_token}";

        $post_arr = [
            "openid"=>$openid,
            "to_groupid"=>$tagid,
        ];

        $post = json_encode($post_arr,JSON_UNESCAPED_UNICODE);

        $return = $this->requestAndCheck($url,'POST',$post);
        if($return === false){
            return false;
        }

        return $return;
    }


    /**
     * 批量为用户打标签
     * @param array $openid_list
     * @param int $tagid
     * @return boolean
     */
    public function batchUsersTag($openid_list,$tagid){
        $access_token = $this->flushAuthorCode();
        if (!$access_token) {
            return false;
        }

        $url = "https://api.weixin.qq.com/cgi-bin/tags/members/batchtagging?access_token={$access_token}";

        $post_arr = [
            "openid_list"=>$openid_list,
            "tagid"=>$tagid,
        ];

        $post = json_encode($post_arr,JSON_UNESCAPED_UNICODE);

        $return = $this->requestAndCheck($url,'POST',$post);
        if($return === false){
            return false;
        }

        return $return;
    }

    /**
     * 批量为用户取消标签
     * @param array $openid_list
     * @param int $tagid
     * @return boolean
     */
    public function batchCancelUsersTag($openid_list,$tagid){
        $access_token = $this->flushAuthorCode();
        if (!$access_token) {
            return false;
        }

        $url = "https://api.weixin.qq.com/cgi-bin/tags/members/batchuntagging?access_token={$access_token}";

        $post_arr = [
            "openid_list"=>$openid_list,
            "tagid"=>$tagid,
        ];

        $post = json_encode($post_arr,JSON_UNESCAPED_UNICODE);

        $return = $this->requestAndCheck($url,'POST',$post);
        if($return === false){
            return false;
        }

        return $return;
    }

    /**
     * 获取用户身上的标签列表
     * @param type $openid
     * @return array
     */
    public function getUserTags($openid){
        $access_token = $this->flushAuthorCode();
        if (!$access_token) {
            return false;
        }

        $url = "https://api.weixin.qq.com/cgi-bin/tags/getidlist?access_token={$access_token}";

        $post_arr = [
            "openid"=>$openid,
        ];

        $post = json_encode($post_arr,JSON_UNESCAPED_UNICODE);

        $return = $this->requestAndCheck($url,'POST',$post);
        if($return === false){
            return false;
        }

        return $return;
    }

    
    /**
     * 获取粉丝标签
     * @return type
     */
    public function getAllFanTags()
    {
        $access_token = $this->flushAuthorCode();
        if (!$access_token) {
            return false;
        }
        
        $url = "https://api.weixin.qq.com/cgi-bin/tags/get?access_token={$access_token}";
        $return = $this->requestAndCheck($url, 'GET');
        if ($return === false) {
            return false;
        }
        
        //$wxdata数据样例：{"tags":[{"id":1,"name":"每天一罐可乐星人","count":0/*此标签下粉丝数*/}, ...]}
        return $return['tags'];
    }
    
    /**
     * 获取所有用户标签
     * @return array
     */
    public function getAllFanTagsMap()
    {
        if ($this->tagsMap !== null) {
            return $this->tagsMap;
        }
        
        $user_tags = $this->getAllFanTags();
        if ($user_tags === false) {
            return false;
        }
        
        $this->tagsMap = [];
        foreach ($user_tags as $tag) {
            $this->tagsMap[$tag['id']] = $this->tagsMap[$tag['name']];
        }
        return $this->tagsMap;
    }
    
    /**
     * 获取粉丝标签名
     * @param string $tagid_list
     * @param array $tagsMap
     * @return array
     */
    public function getFanTagNames($tagid_list)
    {
        if ($this->tagsMap === null) {
            $tagsMap = $this->getAllFanTagsMap();
            if ($tagsMap === false) {
                return false;
            }
            $this->tagsMap = $tagsMap;
        }
        
        $tag_names = [];
        foreach ($tagid_list as $tag) {
            $tag_names[] = $this->tagsMap[$tag];
        }
        return $tag_names;
    }
 
    /**
     * 获取粉丝id列表
     * @param string $next_openid 下一次拉取的起始id的前一个id
     * @return array
     */
    public function getFanIdList($next_openid='')
    {
        $access_token = $this->flushAuthorCode();
        if (!$access_token) {
            return false;
        }
        
        $url ="https://api.weixin.qq.com/cgi-bin/user/get?access_token={$access_token}&next_openid={$next_openid}";//重头开始拉取，一次最多拉取10000个
        $return = $this->requestAndCheck($url, 'GET');
        if ($return === false) {
            return false;
        }
        
        //$list[]元素：
        //total	关注该公众账号的总用户数
        //count	拉取的OPENID个数，最大值为10000
        //data	列表数据，OPENID的列表
        //next_openid	拉取列表的最后一个用户的OPENID
        //样本数据：{"total":2,"count":2,"data":{"openid":["OPENID1","OPENID2"]},"next_openid":"NEXT_OPENID"}
        return $return;
    }

    /**
     * 批量获取用户基本信息
     */
    public function getBatchget($user_arr){
        $access_token = $this->flushAuthorCode();
        if (!$access_token) {
            return false;
        }

        $url = "https://api.weixin.qq.com/cgi-bin/user/info/batchget?access_token={$access_token}";

        $post_arr = [
            "user_list" => $user_arr,
        ];
        $post = json_encode($post_arr , JSON_UNESCAPED_UNICODE);
        $return = $this->requestAndCheck($url , 'POST' , $post);
        if($return === false){
            return false;
        }

        return $return;
    }
    
    /**
     * 设置粉丝备注
     */
    public function setFanRemark($openid, $remark)
    {
        $access_token = $this->flushAuthorCode();
        if (!$access_token) {
            return false;
        }
        
        $post = json_encode(['openid '=> $openid, 'remark' => $remark], JSON_UNESCAPED_UNICODE);
        $url ="https://api.weixin.qq.com/cgi-bin/user/info/updateremark?access_token={$access_token}";
        $return = $this->requestAndCheck($url, 'POST', $post);
        if ($return === false) {
            return false;
        }
        
        return true;
    }

    /**
     * 黑名单管理
     */
    public function getBlackList($beginOpenid = ''){
        $access_token = $this->flushAuthorCode();
        if (!$access_token) {
            return false;
        }

        $url = "https://api.weixin.qq.com/cgi-bin/tags/members/getblacklist?access_token={$access_token}";

        $post_arr = [
            'begin_openid'=>$beginOpenid,
        ];

        $post = json_encode($post_arr,JSON_UNESCAPED_UNICODE);
        $return  = $this->requestAndCheck($url,'POST',$post);
        if($return === false){
            return false;
        }

        return $return;
    }
    
    /*
     * 向一个粉丝发送消息
     */
    public function sendMsgToOne($openid, $content)
    {
        $access_token = $this->flushAuthorCode();
        
        if (!$access_token) {
            return false;
        }
        
        $url ="https://api.weixin.qq.com/cgi-bin/message/custom/send?access_token={$access_token}";        
        $post_arr = [
                        'touser' => $openid,
                        'msgtype' => 'text',
                        'text' => ['content'=>$content]
                    ];
        $post = json_encode($post_arr, JSON_UNESCAPED_UNICODE);        
        $return = $this->requestAndCheck($url, 'POST', $post);
        if ($return === false) {
            return false;
        }
        
        return true;
    }

    /*
     * 检测接入检测api文本回复粉丝发送消息
     */
    public function sendMsgToOneCheck($access_token ,$openid, $content)
    {
        if (!$access_token) {
            return false;
        }
        
        $url ="https://api.weixin.qq.com/cgi-bin/message/custom/send?access_token={$access_token}";        
        $post_arr = [
                        'touser' => $openid,
                        'msgtype' => 'text',
                        'text' => ['content'=>$content]
                    ];
        $post = json_encode($post_arr, JSON_UNESCAPED_UNICODE);        
        $return = $this->requestAndCheck($url, 'POST', $post);
        if ($return === false) {
            return false;
        }
        return true;
    }

    /*
     * 指定客服向一个粉丝发送文本消息
     */
    public function appointSendMsgToOne($openid, $content,$customerService)
    {
        $access_token = $this->flushAuthorCode();
        if (!$access_token) {
            return false;
        }
        
        $url ="https://api.weixin.qq.com/cgi-bin/message/custom/send?access_token={$access_token}";        
        $post_arr = [
                        'touser' => $openid,
                        'msgtype' => 'text',
                        'text' => ['content'=>$content],
                        'customservice' => ['kf_account' => $customerService],
                    ];
        $post = json_encode($post_arr, JSON_UNESCAPED_UNICODE);        
        $return = $this->requestAndCheck($url, 'POST', $post);
        if ($return === false) {
            return false;
        }
        
        return true;
    }

    /*
     * 向一个粉丝发送资源消息
     */
    public function sendMediaToOne($openid, $media_id, $type)
    {
        $access_token = $this->flushAuthorCode();
        if (!$access_token) {
            return false;
        }
        
        $url ="https://api.weixin.qq.com/cgi-bin/message/custom/send?access_token={$access_token}";        
        $post_arr = [
                        'touser' => $openid,
                        'msgtype' => $type,
                        $type => ['media_id'=>$media_id]
                    ];
        $post = json_encode($post_arr, JSON_UNESCAPED_UNICODE);        
        $return = $this->requestAndCheck($url, 'POST', $post);
        if ($return === false) {
            return false;
        }
        
        return true;
    }

    /*
     * 指定客服向一个粉丝资源消息
     */
    public function appointSendMediaToOne($openid, $media_id, $type, $customerService)
    {
        $access_token = $this->flushAuthorCode();
        if (!$access_token) {
            return false;
        }
        
        $url ="https://api.weixin.qq.com/cgi-bin/message/custom/send?access_token={$access_token}";        
        $post_arr = [
                        'touser' => $openid,
                        'msgtype' => $type,
                        $type => ['media_id'=>$media_id],
                        'customservice' => ['kf_account' => $customerService],
                    ];
        $post = json_encode($post_arr, JSON_UNESCAPED_UNICODE);        
        $return = $this->requestAndCheck($url, 'POST', $post);
        if ($return === false) {
            return false;
        }
        
        return true;
    }

    /*
     * 向一个粉丝发送图文消息
     */
    public function sendImgMsgToOne($openid, $media_id)
    {
        $access_token = $this->flushAuthorCode();
        if (!$access_token) {
            return false;
        }
        
        $url ="https://api.weixin.qq.com/cgi-bin/message/custom/send?access_token={$access_token}";        
        $post_arr = [
                        'touser' => $openid,
                        'msgtype' => 'mpnews',
                        'mpnews' => ['media_id'=>$media_id]
                    ];
        $post = json_encode($post_arr, JSON_UNESCAPED_UNICODE);        
        $return = $this->requestAndCheck($url, 'POST', $post);
        if ($return === false) {
            return false;
        }
        
        return true;
    }
    
    /**
     * 指定一部分人群发消息
     * @param array or string $openids
     * @param string $content
     * @return boolean
     */
    public function sendMsgToMass($openids, $content)
    {
        $access_token = $this->flushAuthorCode();
        if (!$access_token) {
            return false;
        }
        
        $url ="https://api.weixin.qq.com/cgi-bin/message/mass/send?access_token={$access_token}";
        $post_arr = [
                        'touser' => $openids,
                        'msgtype' => 'text',
                        'text' => ['content'=>$content]
                    ];
        $post = json_encode($post_arr, JSON_UNESCAPED_UNICODE);        
        $return = $this->requestAndCheck($url, 'POST', $post);
        if ($return === false) {
            return false;
        }
        
        return true;
    }
    
    /**
     * 给所有粉丝发消息
     * @param string $content
     * @return boolean
     */
    public function sendMsgToAll($content)
    {
        $access_token = $this->flushAuthorCode();
        if (!$access_token) {
            return false;
        }

        $url ="https://api.weixin.qq.com/cgi-bin/message/mass/sendall?access_token={$access_token}";        
        $post_arr = [
                        'filter' => ['is_to_all'=>true, 'tag_id'=>0],
                        'msgtype' => 'text',
                        'text' => ['content'=>$content]
                    ];
        $post = json_encode($post_arr, JSON_UNESCAPED_UNICODE);        
        $return = $this->requestAndCheck($url, 'POST', $post);
        if ($return === false) {
            return false;
        }
        
        return true;
    }

    
    /**
     * 发送消息，自动识别id数
     * @param string or array $openids
     * @param string $content
     * @return boolean
     */
    public function sendMsg($openids, $content)
    {
        if (empty($openids)) {
            return true;
        }
        if (is_string($openids)) {
            $openids = explode(',', $openids);
        }
        
        if (count($openids) > 1) {
            $result = $this->sendMsgToMass($openids, $content);
        } else {
            $result = $this->sendMsgToOne($openids[0], $content);
        }        
        if ($result === false) {
            return false;
        }
        
        return true;
    }
    
    /**
     * 新增媒质永久素材
     * 文档：https://mp.weixin.qq.com/wiki?action=doc&id=mp1444738729
     * @parem type $path 素材地址
     * @param string $type 类型有image,voice,video,thumb
     * @param array $param 目前是video类型需要
     * @return {"media_id":MEDIA_ID,"url":URL}
     */
    public function uploadMaterial($path, $type = 'news', $param=[])
    {
        $access_token = $this->flushAuthorCode();
        if (!$access_token) {
            return false;
        }
        
        $post_arr = ['media' => '@'.$path];
        if ($type == 'video') {
            $description = [
                'title' => $param['title'],
                'introduction' => $param['introduction'],
            ];
            $post_arr['description'] = json_encode($description, JSON_UNESCAPED_UNICODE);
        }
        
        $url ="https://api.weixin.qq.com/cgi-bin/material/add_material?access_token={$access_token}&type={$type}";
        // return $post_arr['media'];
        $return = $this->requestAndCheck($url, 'POST', $post_arr);
        if ($return === false) {
            return false;
        }
        
        return $return;
    }
    
    /**
     * 上传图文素材。 说明：news里面的图片只能用news_image，封面用image
     * 文档：https://mp.weixin.qq.com/wiki?action=doc&id=mp1444738729
     * @param array $articles
     *  [
     *      [
     *          "title"=> TITLE,
     *          "thumb_media_id"=> THUMB_MEDIA_ID,
     *          "author"=> AUTHOR,
     *          "digest"=> DIGEST,
     *          "show_cover_pic"=> SHOW_COVER_PIC(0 / 1),
     *          "content"=> CONTENT,
     *          "content_source_url"=> CONTENT_SOURCE_URL
     *      ],
     *      //若新增的是多图文素材，则此处应还有几段articles结构(最多8段)
     *  ]
     * @return MEDIA_ID
     */
    public function uploadNews($articles)
    {
        $access_token = $this->flushAuthorCode();
        if (!$access_token) {
            return false;
        }
        
        $post_arr = ["articles"=>$articles];  
        $post = json_encode($post_arr, JSON_UNESCAPED_UNICODE);

        $url ="https://api.weixin.qq.com/cgi-bin/material/add_news?access_token={$access_token}";
        $return = $this->requestAndCheck($url, 'POST', $post);
        if ($return === false) {
            return false;
        }
        
        return $return['media_id'];
    }
    
    /**
     * 上传图文消息中的图片
     * 文档：https://mp.weixin.qq.com/wiki?action=doc&id=mp1444738729
     * @param type $path 图片地址
     * @return 图片的url
     */
    public function uploadNewsImage($path)
    {
        $access_token = $this->flushAuthorCode();
        if (!$access_token) {
            return false;
        }
        
        $post_arr = ["media"=>'@'.$path];  
        $url ="https://api.weixin.qq.com/cgi-bin/media/uploadimg?access_token={$access_token}";
        $return = $this->requestAndCheck($url, 'POST', $post_arr);
        if ($return === false) {
            return false;
        }

        return $return['url'];
    }

    /**
     * 上传临时材料（3天内有效）
     * 文档：https://mp.weixin.qq.com/wiki?action=doc&id=mp1444738726
     * @parem type $path 素材地址
     * @param string $type 类型有image,voice,video,thumb
     * @return {"type":"TYPE","media_id":"MEDIA_ID","created_at":123456789}
     */
    public function uploadTempMaterial($path, $type = 'image')
    {
        if (!($access_token = $this->flushAuthorCode())) {
            return false;
        }
        
        $post_arr = ['media' => '@'.$path];  
        $url ="https://api.weixin.qq.com/cgi-bin/media/upload?access_token={$access_token}&type={$type}";
        $return = $this->requestAndCheck($url, 'POST', $post_arr);
        if ($return === false) {
            return false;
        }
        
        return $return;
    }
    
    /**
     * 更新一篇图文
     * 文档：https://mp.weixin.qq.com/wiki?action=doc&id=mp1444738732&t=0.5904919423628598
     * @param string $mediaId MEDIA_ID 
     * @param array $article INDEX
       {
            "title": TITLE,
            "thumb_media_id": THUMB_MEDIA_ID,
            "author": AUTHOR,
            "digest": DIGEST,
            "show_cover_pic": SHOW_COVER_PIC(0 / 1),
            "content": CONTENT,
            "content_source_url": CONTENT_SOURCE_URL
        }
     * @param number $index 要更新的文章在图文消息中的位置（多图文消息时，此字段才有意义），第一篇为0
     * @return boolean
     */
    public function updateNews($mediaId, $article, $index = 0)
    {
        $access_token = $this->flushAuthorCode();
        if (!$access_token) {
            return false;
        }
        
        $post_arr = [
            'media_id' => $mediaId,
            'index' => $index,
            'articles' => $article
        ];
        $post = json_encode($post_arr, JSON_UNESCAPED_UNICODE);

        $url ="https://api.weixin.qq.com/cgi-bin/material/update_news?access_token={$access_token}";
        $return = $this->requestAndCheck($url, 'POST', $post);
        if ($return === false) {
            return false;
        }
        
        return true;
    }
    
    /**
     * 获取图文素材
     * @param type $mediaId
     * @return boolean
     */
    public function getNews($mediaId)
    {
        $wxdata = $this->getMaterial($mediaId);
        if ($wxdata === false) {
            return false;
        }
        
//    [
//        [
//        title 图文消息的标题
//        thumb_media_id	图文消息的封面图片素材id（必须是永久mediaID）
//        show_cover_pic	是否显示封面，0为false，即不显示，1为true，即显示
//        author	作者
//        digest	图文消息的摘要，仅有单图文消息才有摘要，多图文此处为空
//        content	图文消息的具体内容，支持HTML标签，必须少于2万字符，小于1M，且此处会去除JS
//        url	图文页的URL
//        content_source_url	图文消息的原文地址，即点击“阅读原文”后的URL
//        ],
//        //多图文消息有多篇文章
//     ]
        return $wxdata['news_item'];
    }

    /**
     * 获取临时媒质素材
     * @param type $mediaId
     */
    public function getTemporaryMaterial($mediaId){
        $access_token = $this->flushAuthorCode();
        if (!$access_token) {
            return false;
        }

        $url ="https://api.weixin.qq.com/cgi-bin/media/get?access_token={$access_token}&media_id={$mediaId}";

        $return = $this->downloadMediaSource($url);
        if ($return === false) {
            return false;
        }
        return $return;
    }

    
    /**
     * 获取媒质素材
     * @param type $mediaId
     * @return boolean
        array video返回{
         "title":TITLE,
         "description":DESCRIPTION,
         "down_url":DOWN_URL,
        }
     */
    public function getMaterial($mediaId)
    {
        $access_token = $this->flushAuthorCode();
        if (!$access_token) {
            return false;
        }
        
        $post_arr = ['media_id' => $mediaId];
        $post = json_encode($post_arr, JSON_UNESCAPED_UNICODE);

        $url ="https://api.weixin.qq.com/cgi-bin/material/get_material?access_token={$access_token}";
        $return = $this->requestAndCheck($url, 'POST', $post);
        if ($return === false) {
            return false;
        }
        return $return;
    }
    
    /**
     * 删除素材，包括图文
     * @param type $mediaId
     * @return boolean
     */
    public function delMaterial($mediaId)
    {
        $access_token = $this->flushAuthorCode();
        if (!$access_token) {
            return false;
        }
        
        $post_arr = ['media_id' => $mediaId];
        $post = json_encode($post_arr, JSON_UNESCAPED_UNICODE);

        $url ="https://api.weixin.qq.com/cgi-bin/material/del_material?access_token={$access_token}";
        $return = $this->requestAndCheck($url, 'POST', $post);
        if ($return === false) {
            return false;
        }

        return true;
    }
    
    /**
     * 获取素材总数
     * @return array
        //voice_count	语音总数量
        //video_count	视频总数量
        //image_count	图片总数量
        //news_count	图文总数量
     */
    public function getMaterialCount()
    {
        $access_token = $this->flushAuthorCode();
        if (!$access_token) {
            return false;
        }
        
        $url ="https://api.weixin.qq.com/cgi-bin/material/get_materialcount?access_token={$access_token}";
        $return = $this->requestAndCheck($url, 'GET');
        if ($return === false) {
            return false;
        }

        return $return;
    }
    
    /**
     * 获取素材列表 
     * @param string $type 素材的类型，图片（image）、视频（video）、语音 （voice）、图文（news）
     * @param int $offset 从全部素材的该偏移位置开始返回，0表示从第一个素材 返回
     * @param int $count 返回素材的数量，取值在1到20之间
     * @return array
     */
    public function getMaterialList($type, $offset, $count)
    {
        $access_token = $this->flushAuthorCode();
        if (!$access_token) {
            return false;
        }
        
        $post_arr = [
            'type' => $type,
            'offset' => $offset,
            'count' => $count
        ];
        $post = json_encode($post_arr, JSON_UNESCAPED_UNICODE);
        
        $url ="https://api.weixin.qq.com/cgi-bin/material/batchget_material?access_token={$access_token}";
        $return = $this->requestAndCheck($url, 'POST', $post);
        if ($return === false) {
            return false;
        }

        /* 返回图文消息结构 */
        //{
        //  "total_count": TOTAL_COUNT,
        //  "item_count": ITEM_COUNT,
        //  "item": [{
        //      "media_id": MEDIA_ID,
        //      "content": {
        //          "news_item": [{
        //              "title": TITLE,
        //              "thumb_media_id": THUMB_MEDIA_ID,
        //              "show_cover_pic": SHOW_COVER_PIC(0 / 1),
        //              "author": AUTHOR,
        //              "digest": DIGEST,
        //              "content": CONTENT,
        //              "url": URL,
        //              "content_source_url": CONTETN_SOURCE_URL
        //          },
        //          //多图文消息会在此处有多篇文章
        //          ]
        //       },
        //       "update_time": UPDATE_TIME
        //   },
        //   //可能有多个图文消息item结构
        // ]
        //}
        
        /*其他类型*/
        //{
        //  "total_count": TOTAL_COUNT,
        //  "item_count": ITEM_COUNT,
        //  "item": [{
        //      "media_id": MEDIA_ID,
        //      "name": NAME,
        //      "update_time": UPDATE_TIME,
        //      "url":URL
        //  },
        //  //可能会有多个素材
        //  ]
        //}
        return $return;
    }
    
    /**
     * 创建临时二维码
     * @param int $expire 过期时间，单位秒，最大30天，即2592000秒
     * @param int $scene_id 场景id，用户自定义，目前支持1-100000
     * @param int $scene_str 场景值ID（字符串形式的ID），字符串类型，长度限制为1到64
     * @return boolean
     */
    public function createTempQrcode($expire,$scene_id)
    {
        $access_token = $this->flushAuthorCode();
        if (!$access_token) {
            return false;
        }
        
        $post_arr = [
            'expire_seconds' => $expire,
            'action_name'    => 'QR_SCENE',
            'action_info'    => [
                'scene' => [
                    'scene_id' => $scene_id,
                ]
            ]
        ];
        $post = json_encode($post_arr, JSON_UNESCAPED_UNICODE);
        
        $url ="https://api.weixin.qq.com/cgi-bin/qrcode/create?access_token={$access_token}";
        $return = $this->requestAndCheck($url, 'POST', $post);
        if ($return === false) {
            return false;
        }
        
//        返回数据格式：
//        {
//            "ticket":"gQH47joAAAAAAAAAASxodHRwOi8vd2VpeGluLnFxLmNvbS9xL2taZ2Z3TVRtNzJXV1Brb3ZhYmJJAAIEZ23sUwMEmm3sUw==",
//            "expire_seconds":60,
//            "url":"http:\/\/weixin.qq.com\/q\/kZgfwMTm72WWPkovabbI"
//        }
        
        return $return;
    }

    /**
     * 创建永久二维码
     * @param int $expire 过期时间，单位秒，最大30天，即2592000秒
     * @param int $scene_id 场景id，用户自定义，目前支持1-100000
     * @param int $scene_str 场景值ID（字符串形式的ID），字符串类型，长度限制为1到64
     * @return boolean
     */
    public function createPermQrcode($scene_id)
    {
        $access_token = $this->flushAuthorCode();
        if (!$access_token) {
            return false;
        }
        
        $post_arr = [
            'action_name'    => 'QR_LIMIT_SCENE',
            'action_info'    => [
                'scene' => [
                    'scene_id' => $scene_id
                ]
            ]
        ];
        $post = json_encode($post_arr, JSON_UNESCAPED_UNICODE);
        
        $url ="https://api.weixin.qq.com/cgi-bin/qrcode/create?access_token={$access_token}";
        $return = $this->requestAndCheck($url, 'POST', $post);
        if ($return === false) {
            return false;
        }
        
//        返回数据格式：
//        {
//            "ticket":"gQH47joAAAAAAAAAASxodHRwOi8vd2VpeGluLnFxLmNvbS9xL2taZ2Z3TVRtNzJXV1Brb3ZhYmJJAAIEZ23sUwMEmm3sUw==",
//            "expire_seconds":60,
//            "url":"http:\/\/weixin.qq.com\/q\/kZgfwMTm72WWPkovabbI"
//        }
        
        return $return;
    }

    /**
     * 通过ticket换取二维码
     * @param string $ticket
     */
    public function showQrcode($ticket){
        $url ="https://mp.weixin.qq.com/cgi-bin/showqrcode?ticket={$ticket}";

        $return = $this->downloadMediaSource($url);
        if ($return === false) {
            return false;
        }
        return $return;
    }
    
    /**
     * 推送消息处理接口
     * @return type
     */
    public function handleMessage()
    {
        $wechat = $this->config;
        include_once(EXTEND_PATH.'wxcrypt/wxBizMsgCrypt.php');
        //encodingAesKey和token均为申请三方平台是所填写的内容 
        $encodingAesKey = $wechat['encoding_aes_key'];
        $token = $wechat['w_token'];
        $appId = $wechat['appid'];
        $timeStamp = empty ( $_GET ['timestamp'] ) ? "" : trim ($_GET ['timestamp'] ); 
        $nonce = empty ( $_GET ['nonce'] ) ? "" : trim ( $_GET['nonce'] ); 
        $msg_sign = empty ( $_GET ['msg_signature'] ) ? "" : trim( $_GET ['msg_signature'] ); 
        $pc = new \WXBizMsgCrypt($token, $encodingAesKey, $appId); 
        //获取到微信推送过来post数据（xml格式） 
        $postArr = $GLOBALS['HTTP_RAW_POST_DATA']; 
        $msg = ''; 
        $errCode = $pc->decryptMsg($msg_sign, $timeStamp, $nonce, $postArr,$msg);
        // file_put_contents('./wechatLog.txt',$postArr,FILE_APPEND);
        if($errCode === 0){
            $postObj = \app\common\util\XML::parse($msg);
            return $postObj;
        }else{
            $this->setError('推送消息为空！');
            return false;
        }
        


        // $content = array_key_exists('HTTP_RAW_POST_DATA',$GLOBALS);
        // if (!$content) {
        //     $content = file_get_contents("php://input");
        // }
        
        // $this->debug && $this->logDebugFile($content);

        // $message = \app\common\util\XML::parse($content);
        // if (empty($message)) {
        //     $this->setError('推送消息为空！');
        //     return false;
        // }
        
        // return $message;
    }

    
    /**
     * 创建文本回复消息
     * @param type $fromUser
     * @param type $toUser
     * @param type $text
     * @return type
     */
    public function createReplyMsgOfText($fromUser, $toUser, $text)
    {
        $time = time();
        $template = 
            "<xml>
            <ToUserName><![CDATA[$toUser]]></ToUserName>
            <FromUserName><![CDATA[$fromUser]]></FromUserName>
            <CreateTime>$time</CreateTime>
            <MsgType><![CDATA[text]]></MsgType>
            <Content><![CDATA[$text]]></Content>
            </xml>";
        // file_put_contents('./wechatLog.txt',1,FILE_APPEND);
        // file_put_contents('./wechatLog.txt',$template,FILE_APPEND);
        $template = $this->cryptMsg($template);
        return $template;    
    }
    
    /**
     * 创建图片回复消息
     * @param type $fromUser
     * @param type $toUser
     * @param type $mediaId
     * @return type
     */
    public function createReplyMsgOfImage($fromUser, $toUser, $mediaId)
    {
        $time = time();
        $template = 
            "<xml>
            <ToUserName><![CDATA[$toUser]]></ToUserName>
            <FromUserName><![CDATA[$fromUser]]></FromUserName>
            <CreateTime>$time</CreateTime>
            <MsgType><![CDATA[image]]></MsgType>
            <Image>
            <MediaId><![CDATA[$mediaId]]></MediaId>
            </Image>
            </xml>";
        $template = $this->cryptMsg($template);
        return $template;    
    }
    
    /**
     * 创建图文回复消息
     * @param type $fromUser
     * @param type $toUser
     * @param type $articles
     * @return string
     */
    public function createReplyMsgOfNews($fromUser, $toUser, $articles)
    {
        $articles = array_slice($articles, 0, 7);//最多支持7个
        $num = count($articles);
        if (!$num) {
            return '';
        }
        
        $itemTpl = '';
        foreach ($articles as $item) {
            $itemTpl .= 
            "<item>
            <Title><![CDATA[{$item['title']}]]></Title> 
            <Description><![CDATA[{$item['description']}]]></Description>
            <PicUrl><![CDATA[{$item['picurl']}]]></PicUrl>
            <Url><![CDATA[{$item['url']}]]></Url>
            </item>";
        }
        
        $time = time();
        $template = 
            "<xml>
            <ToUserName><![CDATA[$toUser]]></ToUserName>
            <FromUserName><![CDATA[$fromUser]]></FromUserName>
            <CreateTime>$time</CreateTime>
            <MsgType><![CDATA[news]]></MsgType>
            <ArticleCount>$num</ArticleCount>
            <Articles>$itemTpl</Articles>
            </xml>";
        return $template;
    }

    /**
     * 获取所有客服账号
     * @return array
     */
    public function getCustAcNum(){
        $access_token = $this->flushAuthorCode();
        if (!$access_token) {
            return false;
        }

        $url = "https://api.weixin.qq.com/cgi-bin/customservice/getkflist?access_token={$access_token}";

        $return = $this->requestAndCheck($url, 'GET');
        if ($return === false) {
            return false;
        }

        return $return;
    }

    /**
     * 获取在线客服信息
     * @return array
     */
    public function getOnlineCustList(){
        $access_token = $this->flushAuthorCode();
        if (!$access_token) {
            return false;
        }

        $url = "https://api.weixin.qq.com/cgi-bin/customservice/getonlinekflist?access_token={$access_token}";

        $return = $this->requestAndCheck($url, 'GET');
        if ($return === false) {
            return false;
        }

        return $return;
    }

    /**
     * 添加客服账号
     * @param type $kf_account
     * @param type $nickname
     * @return boolean
     */
    public function createCustAcNum($kfAccount,$nickname){
        $access_token = $this->flushAuthorCode();
        if (!$access_token) {
            return false;
        }

        $url = "https://api.weixin.qq.com/customservice/kfaccount/add?access_token={$access_token}";

        $post_arr = [
            'kf_account' => $kfAccount,
            'nickname' => $nickname,
        ];

        $post = json_encode($post_arr, JSON_UNESCAPED_UNICODE);

        $return = $this->requestAndCheck($url, 'POST', $post);
        if ($return === false) {
            return false;
        }

        return $return;
    }

    /**
     * 邀请绑定客服帐号
     * @param type $kf_account
     * @param type $invite_wx
     * @return boolean
     */
    public function iniviteBindCustAcNum($kfAccount,$inviteWx){
        $access_token = $this->flushAuthorCode();
        if (!$access_token) {
            return false;
        }

        $url = "https://api.weixin.qq.com/customservice/kfaccount/inviteworker?access_token={$access_token}";

        $post_arr = [
            'kf_account' => $kfAccount,
            'invite_wx' => $inviteWx,
        ];

        $post = json_encode($post_arr, JSON_UNESCAPED_UNICODE);

        $return = $this->requestAndCheck($url, 'POST', $post);
        
        // if ($return === false) {
        //     return false;
        // }

        return $return;
    }

    /**
     * 设置客服信息
     * @param type $kf_account
     * @param type $nickname
     * @return boolean
     */
    public function updateCustAcNum($kfAccount,$nickname){
        $access_token = $this->flushAuthorCode();
        if (!$access_token) {
            return false;
        }

        $url = "https://api.weixin.qq.com/customservice/kfaccount/update?access_token={$access_token}";

        $post_arr = [
            'kf_account' => $kfAccount,
            'nickname' => $nickname,
        ];

        $post = json_encode($post_arr, JSON_UNESCAPED_UNICODE);

        $return = $this->requestAndCheck($url, 'POST', $post);
        if ($return === false) {
            return false;
        }

        return $return;
    }

    /**
     * 上传客服头像
     * @param type $kf_account
     * @param type $media
     * @return boolean
     */
    public function uploadCustHeadImg($kfAccount,$path){
        $access_token = $this->flushAuthorCode();
        if (!$access_token) {
            return false;
        }

        $post_arr = ['media' => '@'.$path];  
        $url = "https://api.weixin.qq.com/customservice/kfaccount/uploadheadimg?access_token={$access_token}&kf_account={$kfAccount}";

        $return = $this->requestAndCheck($url, 'POST', $post_arr);
        if ($return === false) {
            return false;
        }

        return $return;
    }

    /**
     * 删除客服帐号
     * @param type $kf_account
     * @return boolean
     */
    public function deleteCustAcNum($kfAccount){
        $access_token = $this->flushAuthorCode();
        if (!$access_token) {
            return false;
        }

        $url = "https://api.weixin.qq.com/customservice/kfaccount/del?access_token={$access_token}&kf_account={$kfAccount}";

        $return  = $this->requestAndCheck($url,'GET');

        if($return === false){
            return false;
        }

        return $return;
    }

    /**
     * 创建会话
     * @param type $kf_account
     * @param type $openid
     * @return boolean
     */
    public function CreateConversation($kfAccount,$openid){
        $access_token = $this->flushAuthorCode();
        if (!$access_token) {
            return false;
        }

        $url = "https://api.weixin.qq.com/customservice/kfsession/create?access_token={$access_token}";

        $post_arr = [
            'kf_account' => $kfAccount,
            'openid' => $openid,
        ];

        $post = json_encode($post_arr, JSON_UNESCAPED_UNICODE);

        $return = $this->requestAndCheck($url, 'POST', $post);
        if ($return === false) {
            return false;
        }

        return $return;
    }

    /**
     * 获取客服会话列表
     */
    public function getSessiOnlist(){
        $access_token = $this->flushAuthorCode();
        if (!$access_token) {
            return false;
        }

        $url = "https://api.weixin.qq.com/customservice/kfsession/getsessionlist?access_token={$access_token}&kf_account=test@jmmfds001";

        $return = $this->requestAndCheck($url, 'GET');
        if ($return === false) {
            return false;
        }

        return $return;
    }

    /**
     * 获取聊天记录
     * @param int $start_time
     * @param int $end_time
     * @param int $msgid
     * @param int $number
     * @return array
     */
    public function getWhatRecord($start_time,$end_time,$msgid = 1,$number = 10000){
        $access_token = $this->flushAuthorCode();
        if (!$access_token) {
            return false;
        }

        $url = "https://api.weixin.qq.com/customservice/msgrecord/getmsglist?access_token={$access_token}";

        $post_arr = [
            "starttime"=>$start_time,
            "endtime"=>$end_time,
            'msgid'=>$msgid,
            "number"=>$number,
        ];

        $post = json_encode($post_arr,JSON_UNESCAPED_UNICODE);

        $return = $this->requestAndCheck($url, 'POST', $post);
        if ($return === false) {
            return false;
        }

        return $return;
    }

    /**
     * 获取第三方平台component_access_token
     * {
     *  "component_appid":"appid_value" , //第三方平台appid
     *  "component_appsecret": "appsecret_value", //第三方平台appsecret
     *  "component_verify_ticket": "ticket_value" //微信后台推送的ticket，此ticket会定时推送，具体请见本页的推送说明
     *  }
     */
    public function getComponentToken(){
        $wechat = $this->config;
        if (empty($wechat)) {
            $this->setError("第三方平台不存在！");
            return false;
        } 

        //判断是否过了缓存期
        $expire_time = $wechat['web_expires'];
        if($expire_time > time()){
           return $wechat['component_token'];
        }

        $component_appid = $wechat['appid'];
        $component_appsecret = $wechat['appsecret'];
        $component_verify_ticket = $wechat['component_verify_ticket'];
        $url = 'https://api.weixin.qq.com/cgi-bin/component/api_component_token';

        $post_arr = [
            "component_appid" => $component_appid,
            "component_appsecret" => $component_appsecret,
            "component_verify_ticket" => $component_verify_ticket,
        ];

        $post = json_encode($post_arr,JSON_UNESCAPED_UNICODE);

        $return = $this->requestAndCheck($url, 'POST',$post);
        if ($return === false) {
            return false;
        }

        $web_expires = time() + 7000; // 提前200秒过期
        $this->wx_user->save(['component_token'=>$return['component_access_token'], 'web_expires'=>$web_expires],['id'=>$wechat['id']]);
        $this->config['component_token'] = $return['component_access_token'];
        $this->config['web_expires'] = $web_expires;
        
        return $return['component_access_token'];

    }

    /**
     * 获取（刷新）授权公众号或小程序的接口调用凭据（令牌）
     * {
     *  "component_appid":"appid_value",
     *  "authorizer_appid":"auth_appid_value",
     *  "authorizer_refresh_token":"refresh_token_value",
     * }
     */
    public function flushAuthorCode(){
        $wechat = $this->config; //开发平台配置
        $authorizer_info = $this->author_config; //授权公众号配置
        
        //实时请求access_token过期时间，判断是否过了缓存期，不要用session的过期时间判断
        $author_data = Db::name('author_wx_user')->field('authorizer_access_token,authorizer_expires')->where('id',$authorizer_info['id'])->find();
        $expire_time = $author_data['authorizer_expires'];
        if($expire_time > time()){
            return $author_data['authorizer_access_token'];
        }

        $component_appid = $wechat['appid'];
        $component_token = $this->getComponentToken();
        if (!$component_token) {
            return false;
        }

        $url = 'https://api.weixin.qq.com/cgi-bin/component/api_authorizer_token?component_access_token='.$component_token;

        

        $post_arr = [
            'component_appid' => $component_appid,
            'authorizer_appid' => $authorizer_info['appid'],
            'authorizer_refresh_token' => $authorizer_info['authorizer_refresh_token'],
        ];

        $post = json_encode($post_arr,JSON_UNESCAPED_UNICODE);

        $return = $this->requestAndCheck($url,'POST',$post);

        // file_put_contents('./requestMsg.log','['.date("Y-m-d H:i:s").'] flushAuthorCode:'.$return['authorizer_refresh_token']."\n",FILE_APPEND);

        if($return){
            
            $authorizer_expires = time() + 7000;

            $data = [
                'authorizer_access_token' => $return['authorizer_access_token'],
                'authorizer_refresh_token' => $return['authorizer_refresh_token'],
                'authorizer_expires' => $authorizer_expires,
            ];
            $this->author_wx_user->save($data,['id' => $authorizer_info['id']]);
            $this->author_config['authorizer_access_token'] = $return['authorizer_access_token'];
            $this->author_config['authorizer_expires'] = $authorizer_expires;
    
            /**
             * {
             *  "authorizer_access_token" : "aaUl5s6kAByLwgV0BhXNuIFFUqfrR8vTATsoSHukcIGqJgrc4KmMJ-JlKoC_-NKCLBvuU1cWPv4vDcLN8Z0pn5I45mpATruU0b51hzeT1f8", 
             *  "expires_in" : 7200, 
             *  "authorizer_refresh_token" : "BstnRqgTJBXb9N2aJq6L5hzfJwP406tpfahQeLNxX0w"
             * }
             */
    
            return $return['authorizer_access_token'];
        }else{
            return false;
        }
    }

    /**
     * 公众号调用或第三方代公众号调用对公众号的所有API调用（包括第三方代公众号调用）次数进行清零
     * @param access_token	调用接口凭据
     * @param appid	公众号的APPID
     */
    public function clearQuota(){
        $wechat = $this->config;
        $component_appid = $wechat['appid'];
        $component_token = $this->getComponentToken();
        if (!$component_token) {
            return false;
        }
        $url = 'https://api.weixin.qq.com/cgi-bin/clear_quota?access_token='.$component_token;

        $post_arr = [
            'appid' => $appid,
        ];

        $post = json_encode($post_arr,JSON_UNESCAPED_UNICODE);

        $return = $this->requestAndCheck($url, 'POST', $post);
        if ($return === false) {
            return false;
        }

        return true;
    }

    /**
     * 第三方平台对其所有API调用次数清零（只与第三方平台相关，与公众号无关，接口如api_component_token）
     * @param component_access_token	调用接口凭据
     * @param component_appid	第三方平台APPID
     */
    public function clearQuotaThird(){
        $wechat = $this->config;
        $component_appid = $wechat['appid'];
        $component_token = $this->getComponentToken();
        if (!$component_token) {
            return false;
        }

        $url = 'https://api.weixin.qq.com/cgi-bin/component/clear_quota?component_access_token='.$component_token;

        $post_arr = [
            'component_appid' => $component_appid,
        ];

        $post = json_encode($post_arr , JSON_UNESCAPED_UNICODE);
        $return = $this->requestAndCheck($url ,'POST' , $post);

        if($return === false){
            return false;
        }

        return true;

    }

    /**
     * 获取授权方的帐号基本信息
     */
    public function getAuthorizerInfo(){
        $component_token = $this->getComponentToken();
        if (!$component_token) {
            return false;
        }

        $wechat = $this->config;
        $component_appid = $wechat['appid'];
        $authorizer_appid = $this->author_config['appid'];

        $url = 'https://api.weixin.qq.com/cgi-bin/component/api_get_authorizer_info?component_access_token='.$component_token;

        $post_arr = [
            'component_appid' => $component_appid,
            'authorizer_appid' => $authorizer_appid,
        ];

        $post = json_encode($post_arr , JSON_UNESCAPED_UNICODE);
        $return = $this->requestAndCheck($url,'POST',$post);

        if($return === false){
            return false;
        }

        return $return;
    }

    /**
     * 消息转至普通客服
     */
    public function transmitService($object)
    {
        $xmlTpl = 
            "<xml>
                <ToUserName><![CDATA[%s]]></ToUserName>
                <FromUserName><![CDATA[%s]]></FromUserName>
                <CreateTime>%s</CreateTime>
                <MsgType><![CDATA[transfer_customer_service]]></MsgType>
             </xml>";
        $template = sprintf($xmlTpl, $object['FromUserName'], $object['ToUserName'], time());
        $template = $this->cryptMsg($template);
        return $template;
    }

    /**
     * 消息转至指定客服
     */
    public function transmitExcService($object,$customerService)
    {
        $xmlTpl = 
            "<xml>
                <ToUserName><![CDATA[%s]]></ToUserName>
                <FromUserName><![CDATA[%s]]></FromUserName>
                <CreateTime>%s</CreateTime>
                <MsgType><![CDATA[transfer_customer_service]]></MsgType>
                <TransInfo>
                    <KfAccount>< ![CDATA[%s] ]></KfAccount>
                </TransInfo>
             </xml>";
        $template = sprintf($xmlTpl, $object['FromUserName'], $object['ToUserName'], time(), $customerService);
        $template = $this->cryptMsg($template);
        return $template;
    }

    /**
     * 创建自定义菜单
     */
    public function createMenu($menu_arr){
        $access_token = $this->flushAuthorCode();
        if (!$access_token) {
            return false;
        }

        $post_arr = [
            'button' => $menu_arr,
        ];
        $post = json_encode($post_arr , JSON_UNESCAPED_UNICODE);

        $url = "https://api.weixin.qq.com/cgi-bin/menu/create?access_token={$access_token}";

        // file_put_contents('./requestMsg.log','['.date("Y-m-d H:i:s").'] access_token:'.$access_token."\n",FILE_APPEND);
        // file_put_contents('./requestMsg.log','['.date("Y-m-d H:i:s").'] ReturnPost:'.$post."\n",FILE_APPEND);

        $return = $this->requestAndCheck($url, 'POST', $post);
        if ($return === false) {
            return false;
        }

        return $return;
    }

    /*
     * 获取在线的自定义菜单
     * @aeball  data 20180624
     * http请求方式：GET     https://api.weixin.qq.com/cgi-bin/menu/get?access_token=ACCESS_TOKEN
     * */
    public function getMenu(){
        // 1 获取token
        $wx_token = $this->flushAuthorCode();
        // 2 请求json
        $token_url = "https://api.weixin.qq.com/cgi-bin/menu/get?access_token=" . $wx_token;
        $menu_json = $this->requestAndCheck($token_url,'GET');
        return $menu_json;
    }



    /**
     * ---模板开发模块
     */

    /**
     * 设置所属行业
     * @param type industry1 行业id
     * @param type industry2 行业id
     */
    public function setIndustry($industry1,$industry2){
        $access_token = $this->flushAuthorCode();
        if (!$access_token) {
            return false;
        }

        $url = "https://api.weixin.qq.com/cgi-bin/template/api_set_industry?access_token={$access_token}";

        $post_arr = [
            "industry_id1" => $industry1,
            "industry_id2" => $industry2
        ];

        $post = json_encode($post_arr , JSON_UNESCAPED_UNICODE);

        $return = $this->requestAndCheck($url, 'POST', $post);
        if ($return === false) {
            return false;
        }

        return $return;
    }

    /**
     * 获取设置的行业信息
     * @return primary_industry 帐号设置的主营行业
     * @return secondary_industry 帐号设置的副营行业
     */
    public function getIndustry(){
        $access_token = $this->flushAuthorCode();
        if (!$access_token) {
            return false;
        }

        $url = "https://api.weixin.qq.com/cgi-bin/template/get_industry?access_token={$access_token}";

        $return = $this->requestAndCheck($url, 'GET');
        if ($return === false) {
            return false;
        }
        return $return;
    }

    /**
     * 获得模板ID
     * @param template_id_short 模板库中模板的编号，有“TM**”和“OPENTMTM**”等形式
     */
    public function getTemplateId($template_id){
        $access_token = $this->flushAuthorCode();
        if (!$access_token) {
            return false;
        }

        $url = "https://api.weixin.qq.com/cgi-bin/template/api_add_template?access_token={$access_token}";

        $post_arr = [
            "template_id_short" => $template_id
        ];
        //json_unescaped_unicode
        $post = json_encode($post_arr , JSON_UNESCAPED_UNICODE);

        $return = $this->requestAndCheck($url, 'POST', $post);
        if ($return === false) {
            return false;
        }

        return $return;
    }

    /**
     * 获取模板列表 
     */
    public function getTemplateList(){
        $access_token = $this->flushAuthorCode();
        if (!$access_token) {
            return false;
        }

        $url = "https://api.weixin.qq.com/cgi-bin/template/get_all_private_template?access_token={$access_token}";

        $return = $this->requestAndCheck($url,'GET');
        if ($return === false) {
            return false;
        }

        return $return;
    }

    /**
     * 删除模板
     * @param template_id 公众帐号下模板消息ID
     */
    public function delTemplate($template_id){
        $access_token = $this->flushAuthorCode();
        if (!$access_token) {
            return false;
        }

        $url = "https://api.weixin.qq.com/cgi-bin/template/del_private_template?access_token={$access_token}";

        $post_arr = [
            'template_id' => $template_id,
        ];

        $post = json_encode($post_arr , JSON_UNESCAPED_UNICODE);

        $return = $this->requestAndCheck($url , 'POST', $post);
        if ($return === false) {
            return false;
        }

        return $return;
    }

    /**
     * 发送模板消息
     *       
        {
            "touser":"OPENID",
            "template_id":"ngqIpbwh8bUfcSsECmogfXcV14J0tQlEpBO27izEYtY",
            "url":"http://weixin.qq.com/download",  
            "miniprogram":{
                "appid":"xiaochengxuappid12345",
                "pagepath":"index?foo=bar"
            },          
            "data":{
                    "first": {
                        "value":"恭喜你购买成功！",
                        "color":"#173177"
                    },
                    "keyword1":{
                        "value":"巧克力",
                        "color":"#173177"
                    },
                    "keyword2": {
                        "value":"39.8元",
                        "color":"#173177"
                    },
                    "keyword3": {
                        "value":"2014年9月22日",
                        "color":"#173177"
                    },
                    "remark":{
                        "value":"欢迎再次购买！",
                        "color":"#173177"
                    }
            }
        }
     * 注：url和miniprogram都是非必填字段，若都不传则模板无跳转；若都传，会优先跳转至小程序。
     * 开发者可根据实际需要选择其中一种跳转方式即可。当用户的微信客户端版本不支持跳小程序时，将会跳转至url。
     */
    public function sendTemplateMsg($template_msg){
        $access_token = $this->flushAuthorCode();
        if (!$access_token) {
            return false;
        }

        $url = "https://api.weixin.qq.com/cgi-bin/message/template/send?access_token={$access_token}";

        $post_arr = $template_msg;

        $post = json_encode($post_arr , JSON_UNESCAPED_UNICODE);

        // file_put_contents('./abc.log',$post."\n",FILE_APPEND);
        $return  = $this->requestAndCheck($url , 'POST' , $post);
        if ($return === false) {
            return false;
        }

        return $return;
    }



}



















