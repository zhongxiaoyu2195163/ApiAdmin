<?php
namespace app\api\controller;
/**
 * 标签api
 */

 use app\common\model\UserGroup as UserGroupModel;
 use app\common\util\WechatThirdUtil;
 use app\common\model\WxUser as WxUserModel;
 use app\common\model\AuthorWxUser as AuthorWxUserModel;

 class Label extends Base
 {
    private $author_wx_user_model;
    private $user_group_model;
    private $config;
    private $wx_user;

    protected function _initialize(){
        parent::_initialize();
        $this->user_group_model = new UserGroupModel;
        $this->author_wx_user_model = new AuthorWxUserModel();
        $this->wx_user = new WxUserModel();
        $this->config = $this->wx_user->find();
    }

    /**
     * 查看全部分组
     * @param type auid 授权公众号id
     */
    public function getLabel(){
        $auid = $this->request->post('auid');
        $label = [];
        $label_result_arr = $this->user_group_model->getLabelTopClass($auid);
        return $this->buildSuccess(['status'=> 1,'msg'=>'获取成功','lb_labelList'=>$label_result_arr]);
    }

    /**
     * 获取所有子级标签
     * @param type auid 授权公众号id
     */
    public function getSonLabelInfo(){
        $auid = $this->request->post('auid');
        $where = [
            'pid' => ['gt',0],
            'auid' => ['eq',$auid],
        ];
        $result = $this->user_group_model->where($where)->column('name','group_id');
        return $this->buildSuccess(['status'=> 1,'msg'=>'获取成功','lb_labelSubList'=>$result]);
    }

    /**
     * 创建父级分组
     * @param type auid 授权公众号id
     * @param type name 标签名称
     */
    private function createMainClass(array $data){
            $result = $this->user_group_model->allowField(true)->save($data);
            return $result;
    }

    // public function saveLabel($data){
    //     $result = $this->createMainClass($data);
    //     if($result) return true;
    //     return false;
    // }

    /**
     * 创建父级分组
     * @param type auid 授权公众号id
     * @param type name 标签名称
     */
    public function createGroupCategory(){
        $data = $this->request->post();
        $result = $this->createMainClass($data);
        if($result){
            return $this->buildSuccess(['status'=> 1,'msg'=>'创建成功']);
        }else{
            return json(['status'=>-1,'msg'=>'save error']);
        }
    }
    
    /**
     * 创建子级标签
     * @param type auid 授权公众号id
     * @param type name 标签名称
     * @param type pid 父级分组id
     */
    public function createLabel(){
        $data = $this->request->post();
        if($data['pid'] != 0 && $data['pid'] > 0){
            // 向公众添加标签
            $author_wx = $this->author_wx_user_model->where('id',$data['auid'])->where('authorized',1)->find();
            $wechatObj = new WechatThirdUtil($this->config , $author_wx);
            $tag_result = $wechatObj->createGroups($data['name']);
            //添加成功后写入数据库
            if($tag_result){
                $data['group_id'] = $tag_result['group']['id'];
                $result = $this->user_group_model->allowField(true)->save($data);
                if($result){
                    return $this->buildSuccess(['status'=> 1,'msg'=>'创建成功']);
                }else{
                    return json(['status' => -1,'msg' => '数据库添加失败']);
                }
            }else{
                return json(['status' => -1,'msg' => '公众号添加失败']);
            }
        }
    }

    /**
     * 创建子级标签
     * @param type auid 授权公众号id
     * @param type appid 公众号appid
     * @param type name 标签名称
     * @param type pid 父级标签
     */
    // public function saveSonLabel($auid , $data){
    //     if($data['pid'] != 0 && $data['pid'] > 0){
    //         // 向公众添加标签
    //         $author_wx = $this->author_wx_user_model->where('id',$auid)->where('authorized',1)->find();
    //         $wechatObj = new WechatThirdUtil($this->config , $author_wx);
    //         $tag_result = $wechatObj->createGroups($data['name']);
    //         //添加成功后写入数据库
    //         if($tag_result){
    //             $data['group_id'] = $tag_result['group']['id'];
    //             $data['auid'] = $auid;
    //             $result = $this->user_group_model->allowField(true)->save($data);
    //             if($result){
    //                 return json(['status' => 1,'msg' => '添加成功']);
    //             }else{
    //                 return json(['status' => -1,'msg' => '数据库添加失败']);
    //             }
    //         }else{
    //             return json(['status' => -1,'msg' => '公众号添加失败']);
    //         }
    //     }
    // }

    

    /**
     * 编辑用户分组
     * @param type auid 授权公众号id
     * @param type appid 公众号appid
     * @param type group_id 组id
     * @param type id 标签id
     * @param type name 标签名称
     * @param type pid 父级id
     */
    // public function updateLabel(){
    //     if($this->request->isPost()){
    //         $data = $this->request->post();
    //         if($data['pid'] != 0 && $data['pid'] > 0){
    //             $result = $this->user_group_model->save(['name'=>$data['name']],['auid'=>$data['auid'],'group_id'=>$data['group_id']]);
    //             if($result){
    //                 // 向公众修改标签
    //                 $appid = $data['appid'];
    //                 $author_wx = $this->author_wx_user_model->where('appid',$appid)->where('authorized',1)->find();
    //                 $wechatObj = new WechatThirdUtil($this->config , $author_wx);
    //                 $tag_result = $wechatObj->updateTag($data['group_id'],$data['name']);
    //                 if($tag_result){
    //                     return json(['status' => 1,'msg' => '修改成功']);
    //                 }else{
    //                     return json(['status' => -1,'msg' => '公众号添加失败']);
    //                 }
    //             }else{
    //                 return json(['status' => -1,'msg' => '数据库添加失败']);
    //             }
    //         }elseif($data['pid'] == 0){
    //             //修改父级分类名称
    //             $result = $this->user_group_model->save(['name'=>$data['name']],['auid'=>$data['auid'],'id'=>$data['id'],'pid'=>0]);
    //             if($result){
    //                 return json(['status' => 1,'msg' => '修改成功']);
    //             }else{
    //                 return json(['status' => -1,'msg' => '修改失败']);
    //             }
    //         }
    //     }else{
    //         return json(['status' => -1,'msg' => '错误']);
    //     }
    // }


    /**
     * 编辑用户分组
     * @param type auid 授权公众号id
     * @param type group_id 组id
     * @param type id 标签id
     * @param type name 标签名称
     * @param type pid 父级id
     * @param type color 颜色
     */
    public function updateWechatLabel($data){
        $data = $this->request->post();
        $color = isset($data['color']) ? $data['color'] : '#fff';
        if(isset($data['pid']) && $data['pid'] > 0){
            $result = $this->user_group_model->save(['name'=>$data['name'],'color' => $color],['auid'=>$data['auid'],'group_id'=>$data['group_id']]);
            if($result){
                // 向公众修改标签
                $author_wx = $this->author_wx_user_model->where('id',$data['auid'])->where('authorized',1)->find();
                $wechatObj = new WechatThirdUtil($this->config , $author_wx);
                $tag_result = $wechatObj->updateTag($data['group_id'],$data['name']);
                if($tag_result){
                    return $this->buildSuccess(['status'=> 1,'msg'=>'修改成功']);
                }else{
                    return json(['status' => -1,'msg' => '公众号添加失败']);
                }
            }else{
                return json(['status' => -1,'msg' => '数据库添加失败']);
            }
        }elseif($data['pid'] == 0){
            //修改父级分类名称
            $result = $this->user_group_model->save(['name'=>$data['name'],'color' => $color],['auid'=>$data['auid'],'id'=>$data['id'],'pid' => $data['pid']]);
            if($result){
                return $this->buildSuccess(['status'=> 1,'msg'=>'修改成功']);
            }else{
                return json(['status' => -1,'msg' => '修改失败']);
            }
        }
    }


    /**
     * 删除用户分组
     * @param type auid 授权公众号id
     * @param type appid 公众号appid
     * @param type group_id 组id
     * @param type pid 父级标签
     */
    public function deleteLabel(){
        $data = $this->request->post();
        if($data['pid'] == 0){
            $result = $this->user_group_model->where('pid',$data['id'])->find();
            if($result){
                return json(['status' => -1,'msg' => '删除失败,下级标签未清空']);
            }else{
                $res = $this->user_group_model->where('id',$data['id'])->delete();
                if($res){
                    return $this->buildSuccess(['status'=> 1,'msg'=>'删除成功']);
                }else{
                    return json(['status' => -1,'msg' => '删除失败']);
                }
            }
        }else{
            $result = $this->user_group_model->where('group_id',$data['group_id'])->where('id',$data['id'])->where('auid',$data['auid'])->delete();
            if($result){
                // 向公众号删除标签
                $appid = $data['appid'];
                $author_wx = $this->author_wx_user_model->where('appid',$appid)->where('authorized',1)->find();
                $wechatObj = new WechatThirdUtil($this->config , $author_wx);
                $tag_result = $wechatObj->deleteTag($data['group_id']);
                //删除成功后删除数据库
                if($tag_result){
                    return $this->buildSuccess(['status'=> 1,'msg'=>'删除成功']);
                }else{
                    return json(['status' => -1,'msg' => '公众号删除失败']);
                }
            }else{
                return json(['status' => -1,'msg' => '数据库删除失败']);
            }
        }
    }

    /**
     * 删除用户分组
     * @param type auid 授权公众号id
     * @param type id 标签id
     * @param type group_id 组id
     * @param type pid 父级标签
     */
    public function delLabel(){
        $data = $this->request->post();
        if($data['pid'] == 0){
            $result = $this->user_group_model->where('pid',$data['id'])->find();
            if($result){
                return json(['status' => -1,'msg' => '删除失败,下级标签未清空']);
            }else{
                $res = $this->user_group_model->where('id',$data['id'])->delete();
                if($res){
                    return $this->buildSuccess(['status'=> 1,'msg'=>'删除成功']);
                }else{
                    return json(['status' => -1,'msg' => '删除失败']);
                }
            }
        }else{
            $result = $this->user_group_model->where('group_id',$data['group_id'])->where('id',$data['id'])->where('auid',$data['auid'])->delete();
            if($result){
                // 向公众号删除标签
                $author_wx = $this->author_wx_user_model->where('id',$data['auid'])->where('authorized',1)->find();
                $wechatObj = new WechatThirdUtil($this->config , $author_wx);
                $tag_result = $wechatObj->deleteTag($data['group_id']);
                //删除成功后删除数据库
                if($tag_result){
                    return $this->buildSuccess(['status'=> 1,'msg'=>'删除成功']);
                }else{
                    return json(['status' => -1,'msg' => '公众号删除失败']);
                }
            }else{
                return json(['status' => -1,'msg' => '数据库删除失败']);
            }
        }
    }
 }

