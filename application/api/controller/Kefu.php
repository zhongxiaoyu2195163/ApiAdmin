<?php
namespace app\api\controller;

use think\Controller;
use app\common\model\Kefu as KefuModel;
use app\common\model\Csuser as CsuserModel;

class Kefu extends Controller{
    protected $kefu_model;
    protected $csuser_model;

    protected function _initialize(){
        parent::_initialize();
        $this->kefu_model = new KefuModel;
        $this->csuser_model = new CsuserModel;
    }

    /**
     * 获取账号下的客服列表
     */
    public function SvCtmList($f_ids){
        $csuserList = $this->csuser_model
        ->alias('u')
        ->field('u.id,u.pid,u.auid,u.username,u.nickname,u.header_url,u.status,u.type,u.today_butt,u.online_butt,w.appid,w.wxname')
        ->whereIn('u.id',$f_ids)
        ->join('think_author_wx_user w','u.auid = w.id')
        ->select();

        if($csuserList){
            return $csuserList;
        }else{
            return false;
        }
    }
}