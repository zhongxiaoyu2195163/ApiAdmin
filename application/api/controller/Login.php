<?php
namespace app\api\controller;

use app\api\controller\Kefu;
use app\common\model\Kefu as KefuModel;
use app\common\model\Csuser as CsuserModel;
use think\Config;
// use think\Cache;

class Login extends Base
{
    private $kefu_model;
    private $csuser_model;

    protected function _initialize() {
        parent::_initialize();
        $this->kefu_model = new KefuModel();
        $this->csuser_model = new CsuserModel();
    }
    /**
     * 用户登录
     * @param username 用户名
     * @param password 密码
     */
    public function login(){
        $data = $this->request->post();
        $result = $this->csuser_model->field('think_csuser.*,think_author_wx_user.appid,think_author_wx_user.uid')->where('username',$data['username'])->where('password',md5($data['pwd'] . Config::get('salt')))->join('think_author_wx_user','think_author_wx_user.id = think_csuser.auid')->find();
        // file_put_contents('./requestMsg.log','['.date("Y-m-d H:i:s").'] SqlMsg:'.M('csuser')->getLastSql()."\n",FILE_APPEND);
        if($result){
            // $arr_data = [
            //     'type' => 'current_csuser_count',
            //     'data' => [
            //         'id' => $result['auid'],
            //         'uid' => $result['uid'],
            //     ]
            // ];
            // sendWebSocket($arr_data);
            return $this->buildSuccess(['status'=>1,'msg'=>'登录成功','user_info'=>$result]);
        }else{
            return json_encode(['status'=>-1,'msg'=>'登录失败'],JSON_UNESCAPED_UNICODE);
        }
    }

    /**
     * 在线状态
     * @param id 客服id
     */
    public function online(){
        $id = $this->request->post('id');
        $result = $this->csuser_model->where('id',$id)->update(['type' => 1]);
        if($result){
            $wechat_info = $this->csuser_model->alias('c')->field('a.id,a.uid')->where('c.id',$id)->join('think_author_wx_user a','c.auid = a.id')->find();
            // $arr_data = [
            //     'type' => 'current_csuser_count',
            //     'data' => [
            //         'id' => $wechat_info['id'],
            //         'uid' => $wechat_info['uid'],
            //     ]
            // ];
            // sendWebSocket($arr_data);
            return $this->buildSuccess(['status' => 1,'msg' => '登录成功']);
        }else{
            return json(['status' => -1,'msg' => '操作失败']);
        }
    }

    /**
     * 离线状态
     * @param id 客服id
     * @param return boolean
     */
    public function outline(){
        $id = $this->request->post('id');
        $result = $this->csuser_model->where('id',$id)->update(['type' => 3]);
        if($result){
            $wechat_info = $this->csuser_model->alias('c')->field('a.id,a.uid')->where('c.id',$id)->join('think_author_wx_user a','c.auid = a.id')->find();
            // $arr_data = [
            //     'type' => 'current_csuser_count',
            //     'data' => [
            //         'id' => $wechat_info['id'],
            //         'uid' => $wechat_info['uid'],
            //     ]
            // ];
            // sendWebSocket($arr_data);
            return $this->buildSuccess(['status' => 1,'msg' => '退出成功']);
        }else{
            return json(['status' => -1,'msg' => '操作失败']);
        }
    }

    /**
     * 忙碌状态
     * @param id 客服id
     * @param return boolean
     */
    public function busy(){
        $id = $this->request->post('id');
        $result = $this->csuser_model->where('id',$id)->update(['type' => 2]);
        if($result){
            return $this->buildSuccess(['status'=>1,'msg'=>'忙碌中']);
        }else{
            return json(['status'=>-1,'msg'=>'操作失败']);
        }
    }

    /**
     * 客服手机号码登录
     * @param String username
     * @param String pwd
     */
    public function MobileLogin(){
        $data = $this->request->post();
        $result = $this->kefu_model
                    ->field('id,f_ids,username,nickname')
                    ->where('username',$data['username'])
                    ->where('password',md5($data['pwd'] . Config::get('salt')))
                    ->find();
        if($result){
            $kefu = new Kefu;
            $csuserList = $kefu->SvCtmList($result['f_ids']);
            if($csuserList !== false) $result['csuserList'] = $csuserList;
            $time = time();
            $key = md5(time() . $result['username'] . Config::get('salt'));
            $this->kefu_model->update([
                    'last_login_time' => $time,
                    'last_login_ip'   => $this->request->ip(),
                    'id'              => $result['id'],
                    'key'             => $key,
                ]);
            // Cache::set($result['id'],$key);
            return $this->buildSuccess(['status' => 1,'msg' => '登录成功','user_info' => $result , 'key' => $key]);
        }else{
            return json(['status' => -1,'msg' => '登录失败']);
        }
    }
}